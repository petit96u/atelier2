<?php
require '../vendor/autoload.php';
use photoLocate\backend\controller\AdminController as AdminController;

\photoLocate\common\Api::eloConfigure('../conf/conf.eloquent.ini');

//CHARGEMENT DE SLIM
$app = new \Slim\Slim(['debug'=>true, 'templates.path'=> '../src/photoLocate/backend/templates']);

$app->view = new \Slim\Views\Twig();
$app->view->setTemplatesDirectory('../src/photoLocate/backend/templates');

$view = $app->view();
$view->parserOptions = ['debug'=> true];
$view->parserExtensions = [new \Slim\Views\TwigExtension()];

$app->notFound(function () use ($app) { //si le chemin n'est pas trouvé
	$app->response->headers->set('Content-Type', 'text/html'); //renvoie une réponse en html
	$app->response->setStatus(400);	//avec un statut 400
	$tab = "La route n'existe pas"; //et un tableau renvoyant l'erreur
});

$req = $app->request;
$rootUri = $req->getRootUri();

//ROUTES
$app->get('/', function() use($rootUri){
		$login = new AdminController();
		$login->login($rootUri);
})->name('login');

$app->post('/', function() use($rootUri){
		$login = new AdminController();
		$login->login($rootUri);
});

$app->get('/logout', function() use($rootUri){
    $logout = new AdminController();
    $logout->logout($rootUri);
})->name('logout');

$app->get('/index', function() use ($rootUri){
		$login = new AdminController();
		$login->index($rootUri);
})->name('index');

$app->get('/signin', function() use($rootUri){
		$login = new AdminController();
		$login->addAdministrateur($rootUri);
})->name('signin');

$app->post('/signin', function() use($rootUri){
		$login = new AdminController();
		$login->addAdministrateur($rootUri);
});

$app->get('/photos', function() use ($rootUri){
	$controller = new \photoLocate\backend\controller\PhotoController();
	$datacontroller = $controller->addPhotoInterface($rootUri);
})->name('photos');

$app->post('/photos', function() use ($rootUri){
	$controller = new \photoLocate\backend\controller\PhotoController();
	$datacontroller = $controller->addPhoto($rootUri);
})->name('postPhotos');

$app->get('/series', function() use ($rootUri){ //affichage ajouter une série
	$controller = new \photoLocate\backend\controller\SerieController();
	$dataController = $controller->addSerieAffichage($rootUri);

})->name('series');

$app->post('/series', function() use ($rootUri){ //ajouter une série
	$controller = new \photoLocate\backend\controller\SerieController();
	$dataController = $controller->addSerie($rootUri);

})->name('postSerie');

//EXECUTION / RENDU
$app->run();
?>
