-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Sam 06 Février 2016 à 11:57
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `photolocate`
--

-- --------------------------------------------------------

--
-- Structure de la table `administration`
--

CREATE TABLE IF NOT EXISTS `Administration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(25) DEFAULT NULL,
  `passwd` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Contenu de la table `administration`
--

INSERT INTO `Administration` (`id`, `pseudo`, `passwd`) VALUES
(10, 'admin', '$2y$12$rDAE29IYWXXHfdEARUHbgOvtWVjJyx6IX7/IUJKhP9MOtTgJDqT5e'),
(12, 'nayeli', '$2y$12$u0Tl6J6qS.zwdLIsHX9WH.eJexVkVckKpRqR9AV5At8QIoV/0hYoC'),
(13, 'manon', '$2y$12$thRANUFKF/KsbBeOSFSE2OlbuxpP0ouN.ie8RCHseyJf89G4S.Bqy'),
(15, 'orphee', '$2y$12$.bSKTfbJSsaD7154sNvN1u2.opOns791HmIpD/gnjemEwSY/dq5v6'),
(16, 'myriam', '$2y$12$uwZAwsCxME4w1wBE4K7R5eUtcZFlmTmo0ixFXDAbNLhYBtitNn2Za');

-- --------------------------------------------------------

--
-- Structure de la table `partie`
--

CREATE TABLE IF NOT EXISTS `Partie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(50) DEFAULT NULL,
  `nb_photos` int(11) DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `joueur` varchar(25) DEFAULT NULL,
  `id_serie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60 ;

--
-- Contenu de la table `partie`
--

INSERT INTO `Partie` (`id`, `token`, `nb_photos`, `status`, `score`, `joueur`, `id_serie`) VALUES
(41, 'hchWMGjK4W0cjb4vZ1HOc93Cwj440bFa', NULL, '2', 0, 'Christy', 2),
(42, 'nJen4lHLJ42zFke02VCTZsoaMe9NVCnW', NULL, '2', 0, 'Nayeli', 1),
(49, 'imJxssiguuYfZZLhaM3rxv2BTPNfJrb2', NULL, '2', 112, 'Bob', 1),
(50, 'InQ3Gi4crB7HmA2V2LRXHfKByr76W6hL', NULL, '2', 0, 'tito', 1),
(54, '79qDBAZXEbL23CAsO1ZZyQW90C6bWpZG', NULL, '2', 56, 'toto', 2),
(55, 'AgZcAIs7pBpwEgVq5LIPo1jTZ35q3iEx', NULL, '2', 114, 'john', 1),
(56, 'm3ziT6Urf6lYVeI6sImn5uUhv2mZTizm', NULL, '2', 0, 'Ro', 2),
(57, 'qVPR4CqnzpRGxv9dDDK7SsV33CR2O8Nm', NULL, '2', 104, 'sjegrklhmi', 1),
(58, 'HuTDeXJgwyixSkvW4T3FiTCy3U3vDoEL', NULL, '2', 65, 'Milou', 3),
(59, 'fIv0jVSYzvmvss3ZrBwZODfMd63ctznS', NULL, '2', 0, 'esrdtfy', 1);

-- --------------------------------------------------------

--
-- Structure de la table `photo`
--

CREATE TABLE IF NOT EXISTS `Photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) DEFAULT NULL,
  `latitude` varchar(25) DEFAULT NULL,
  `longitude` varchar(25) DEFAULT NULL,
  `url` varchar(50) DEFAULT NULL,
  `id_Administration` int(11) DEFAULT NULL,
  `id_serie` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Photo_id_Administration` (`id_Administration`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Contenu de la table `photo`
--

INSERT INTO `Photo` (`id`, `description`, `latitude`, `longitude`, `url`, `id_Administration`, `id_serie`) VALUES
(1, '1', '48.691736', '6.17827', '1.jpg', NULL, 1),
(2, '2', '48.6972017', '6.1797089', '2.jpg', NULL, 1),
(3, '3', '48.6936239', '6.1843024', '3.jpg', NULL, 1),
(4, '4', '48.6955', '6.17595', '4.jpg', NULL, 1),
(5, '5', '48.69125', '6.18625', '5.jpg', NULL, 1),
(6, '6', '48.694028', '6.184028', '6.jpg', NULL, 1),
(7, '7', '48.69583', '6.18167', '7.jpg', NULL, 1),
(8, '8', '48.69472', '6.1889', '8.jpg', NULL, 1),
(9, '9', '48.6981957', '6.1742351', '9.jpg', NULL, 1),
(10, '10', '48.698056', '6.184999', '10.jpg', NULL, 1),
(11, '11', '48.873779', '2.295016', '1.jpg', NULL, 2),
(12, '12', '48.868978', '2.310117', '2.jpg', NULL, 2),
(13, '13', '48.866706', '2.305701', '3.jpg', NULL, 2),
(14, '14', '48.866578', '2.311635', '4.jpg', NULL, 2),
(15, '15', '48.870601', '2.316953', '5.jpg', NULL, 2),
(16, '16', '48.863765 ', '2.313573', '6.jpg', NULL, 2),
(17, '17', '48.865986', '2.321966', '7.jpg', NULL, 2),
(18, '18', '48.86398', '2.32533', '8.jpg', NULL, 2),
(19, '19', '48.865748', '2.297519', '9.jpg', NULL, 2),
(20, '20', '48.874083', '2.310332', '10.jpg', NULL, 2),
(21, '56b5c9a797b64.jpg', '51.50611', '-0.12', 'pont.jpg', 10, 3);

-- --------------------------------------------------------

--
-- Structure de la table `score`
--

CREATE TABLE IF NOT EXISTS `Score` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `joueur` varchar(32) NOT NULL,
  `score` int(11) NOT NULL,
  `id_partie` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `score`
--

INSERT INTO `Score` (`id`, `joueur`, `score`, `id_partie`) VALUES
(1, 'Bob', 150, 1),
(2, 'Titi', 251, 2),
(3, 'Bob', 112, 49),
(4, 'toto', 56, 54),
(5, 'john', 114, 55),
(7, 'Milou', 65, 58);

-- --------------------------------------------------------

--
-- Structure de la table `serie`
--

CREATE TABLE IF NOT EXISTS `Serie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ville` varchar(25) DEFAULT NULL,
  `map_lat` varchar(50) DEFAULT NULL,
  `map_lng` varchar(50) NOT NULL,
  `distance` varchar(50) DEFAULT NULL,
  `cover` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Contenu de la table `serie`
--

INSERT INTO `Serie` (`id`, `ville`, `map_lat`, `map_lng`, `distance`, `cover`) VALUES
(1, 'Nancy', '48.6936', '6.1846', '150', 'nancy.jpg'),
(2, 'Paris', '48.868', '2.31061', '150', 'paris.jpg'),
(3, 'Londres', '51.507222', '-0.1275', '150', 'londres.jpg');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
