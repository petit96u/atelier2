<?php

require '../vendor/autoload.php';
require 'checkApiKey.php';
use photoLocate\api\controller\PartiesController as PartiesController;
use photoLocate\api\controller\ScoresController as ScoresController;
use photoLocate\api\controller\SeriesController as SeriesController;
use photoLocate\common\model\Partie as Partie;

\photoLocate\common\Api::EloConfigure('../conf/conf.eloquent.ini');

//CHARGEMENT DE SLIM
$app = new \Slim\Slim();
$app->notFound(function () use ($app) { //si le chemin n'est pas trouvé
	$app->response->headers->set('Content-Type', 'application/json'); //renvoie une réponse en json
	$app->response->setStatus(400);	//avec un statut 400
	$tab = ["Erreur " => "La route n'existe pas"]; //et un tableau renvoyant l'erreur (affiché en json)
	echo json_encode($tab); //affichage du tableau
});

//ROUTES
$app->get('/', function(){ //racine
	echo "Bienvenue sur l'API PhotoLocate ! ";
});

$app->get('/parties/:token', function($token){
    (new PartiesController())->getPartie($token);
})->name('partie');

$app->post('/parties', function() use($app){
    $partie = $app->request->post();
    (new PartiesController())->postParties($partie);
});

$app->put('/parties/:token','checkToken', function($token) use($app){
		$score = $app->request->put('api_score');
		$status = $app->request->put('api_status');
		$serie = $app->request->put('api_id_serie');
		(new PartiesController())->putPartie($token, $status, $score, $serie);
});

/*$app->get('/parties', function(){ //obtenir la collection de parties
    (new PartiesController())->getParties();
})->name('parties');*/

$app->get('/scores','checkToken', function(){ //obtenir les scores
    (new ScoresController())->getScores();
})->name('scores');

$app->get('/series/:id','checkToken', function($id){ //obtenir la ressource série
	$controller = new \photoLocate\api\controller\SeriesController();
	$controller->getSerie($id);

})->name('serie');

$app->get('/series','checkToken', function(){ //obtenir la collection de séries
	$controller = new \photoLocate\api\controller\SeriesController();
	$controller->getSeries();

})->name('series');

$app->get('/series/:id/photos','checkToken', function($id){ //obtenir les photos d'une série
	$controller = new \photoLocate\api\controller\SeriesController();
	$controller->getPhotosSerie($id);

})->name('photosSerie');

$app->post('/scores', 'checkToken', function() use($app){
    $score = $app->request->post();
    (new ScoresController())->postScores($score);
});

//Fonction token
function checkToken(){
		$app = \Slim\Slim::getInstance();
		$token = apache_request_headers()["Authorization"];
		$partie = Partie::all()->where("token",$token)->count();

		if($partie == 0){
				$message = array(
						"Error"=>"Unauthorized"
				);
				$app->response->setStatus(401);
				$app->response->headers->set('Content-Type', 'application/json');
				echo json_encode($message);
				$app->stop();
		}
}

//EXECUTION / RENDU
$app->run();
