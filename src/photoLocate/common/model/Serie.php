<?php
namespace photoLocate\common\model;

class Serie extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'Serie';
	protected $primaryKey = 'id'; 
	public $timestamps = false;

	public function parties() {
		return $this->hasMany('\photoLocate\common\model\Partie', 'id_Serie') ;
	}
		
	public function photos() {
		return $this->hasMany('\racoin\common\model\Photo', 'id_serie') ;
	}		

}