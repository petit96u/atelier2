<?php
namespace photoLocate\common\model;

class Partie extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'Partie';
	protected $primaryKey = 'id'; 
	public $timestamps = false;
	
	public function serie() {
		return $this->belongsTo('\photoLocate\common\model\Serie', 'id_Serie') ; 
	}	

	public function score() {
		return $this->belongsTo('\photoLocate\common\model\Score', 'id_partie') ; 
	}	
}

