<?php
namespace photoLocate\common\model;

class Score extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'Score';
	protected $primaryKey = 'id'; 
	public $timestamps = false;
	
	public function partie() {
		return $this->belongsTo('\photoLocate\common\model\Partie', 'id_partie') ; 
	}	

}

