<?php
namespace photoLocate\common\model;

class Photo extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'Photo';
	protected $primaryKey = 'id'; 
	public $timestamps = false;

	public function serie() {
		return $this->belongsTo('\racoin\common\model\Serie', 'id_serie') ;
	}
	
	public function administrateur(){
		return $this->belongsTo('\racoin\common\model\Administrateur', 'id_administrateur');
	}
	
	
	
}