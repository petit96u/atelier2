<?php
  namespace photoLocate\api\controller;
  use photoLocate\common\model\Partie as Partie;

  class PartiesController{
	public function getPartie($token){
		$app = \Slim\Slim::getInstance();
		$p = Partie::where("token",'=',$token)->first();

		if($p){
			$partie = array(
			"Partie"=>array(
			"id"=>$p->id,
			"token"=>$p->token,
			"nb_photos"=>$p->nb_photos,
			"score"=>$p->score,
			"status"=>$p->status,
			"joueur"=>$p->joueur,
			"id_serie"=>$p->id_serie)
		);

		$app->response->headers->set('Content-Type', 'application/json');
		echo json_encode($partie);
		}else{
			$message = array(
			"Error"=>"Not found"
			);
			$app->response->setStatus(404);
			$app->response->headers->set('Content-Type', 'application/json');
			echo json_encode($message);
		}
	}

      public function postParties($p){
          $app = \Slim\Slim::getInstance();

          $count = Partie::all()->where('joueur', $p['api_joueur'])->count();

          if($count == 0){
              $random = new \RandomLib\Factory;
              $generator = $random->getMediumStrengthGenerator();
              $token = $generator->generateString(32, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');

              $partie = new Partie();
              $partie->token = $token;
              $partie->status = 1;
              $partie->score = 0;
              $partie->joueur = $p['api_joueur'];
              $partie->id_Serie = $p['api_id_serie'];

              if($partie->save()){
                  $message = array(
                      "id" => $partie->id,
                      "token" => $token,
                      "pseudo" => $partie->joueur
                  );
                  $app->response->headers->set('Content-Type','application/json');
                  $app->response->setStatus(201);
                  echo json_encode($message);
              }else{
                  $message = array(
                      "Message"=>"Error. Resource not created"
                  );
                  $app->response->headers->set('Content-Type','application/json');
                  $app->response->setStatus(403);
                  echo json_encode($message);
              }
          }else{
              $message = array(
                  "Error"=>"Pseudo exists"
              );
              $app->response->headers->set('Content-Type','application/json');
              $app->response->setStatus(403);
              echo json_encode($message);
          }



      }

      public function putPartie($token, $status, $score, $serie){
          $app = \Slim\Slim::getInstance();
          $partie = Partie::where("token",'=',$token)->first();

          if($partie){
              /*if($status && $score){
                  $partie->status = $status;
                  $partie->score = $score;
              }elseif($status && !$score){
                  $partie->status = $status;
              }elseif($score && !$status){
                  $partie->score = $score;
              }*/

              if($status){
                  $partie->status = $status;
              }
              if($score){
                  $partie->score = $score;
              }
              if($serie){
                  $partie->id_Serie = $serie;
              }

              if($partie->save()){
                  $message = array(
                      "Message"=>"Resource ".$token." modified"
                  );
                  $app->response->headers->set('Content-Type','application/json');
                  echo json_encode($message);
              }else{
                  $message = array(
                      "Message"=>"Resource ".$token." not modified"
                  );
                  $app->response->setStatus(404);
                  $app->response->headers->set('Content-Type','application/json');
                  echo json_encode($message);
              }
          }else{
              $message = array(
                  "Message"=>"Resource ".$token." not found"
              );
              $app->response->setStatus(404);
              $app->response->headers->set('Content-Type','application/json');
              echo json_encode($message);
          }
      }

      public function getParties() {
          $app = \Slim\Slim::getInstance();
          $parties = Partie::orderBy('score', 'DESC')->get();

          $res = array();

          if($parties) {
              foreach($parties as $partie) {
                  $p = array(
                      "Partie"=>$partie->toArray()
                  );

                  $res[] = $p;
              }
              $app->response->headers->set('Content-Type','application/json');
              echo json_encode($res);
          } else {
              $p = array(
                  "Error"=>"Resources not found"
              );
              $app->response->setStatus(404);
              $app->response->headers->set('Content-Type','application/json');
              echo json_encode($p);
          }
      }
  }
