<?php

namespace photoLocate\api\controller;

use \photoLocate\common\model\Score as Score;

class ScoresController {
	public function getScores() {
		$app = \Slim\Slim::getInstance();
		$scores = Score::orderBy('score', 'DESC')->get();

		$res = array();

		if($scores) {
			foreach($scores as $score) {
				$p = array(
				"Score"=>$score->toArray()
			);
			$res[] = $p;
		}
		$app->response->headers->set('Content-Type','application/json');
		echo json_encode($res);
		} else {
		$s = array(
		"Error"=>"Resources not found"
		);
		$app->response->setStatus(404);
		$app->response->headers->set('Content-Type','application/json');
		echo json_encode($s);
		}
	}
	
	public function postScores($p){
          $app = \Slim\Slim::getInstance();

          $score = new Score();
         /* $partie->token = $p['api_token'];
          $partie->status = 1;
          $partie->score = 0;
          $partie->joueur = $p['api_joueur'];
          $partie->id_Serie = $p['api_id_serie'];*/
		  
		    $score->joueur = $p['joueur']; 
			$score->score = $p['score']; 
			$score->id_partie = $p['partieId'];
			
			
          if($score->save()){
              $message = array(
                  "Message"=>"Resource créée",
                  "Resource"=>['#'],
              );
              $app->response->headers->set('Content-Type','application/json');
              $app->response->setStatus(201);
              echo json_encode($message);
          }else{
              $message = array(
                  "Message"=>"Error. Resource not created"
              );
            $app->response->headers->set('Content-Type','application/json');
            $app->response->setStatus(403);
            echo json_encode($message);
        }
    }
}