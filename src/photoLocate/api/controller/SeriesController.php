<?php

namespace photoLocate\api\controller;

use \photoLocate\common\model\Serie as Serie;
use \photoLocate\common\model\Photo as Photo;

class SeriesController {

	public function getSerie($id) { //obtenir la série
		$app = \Slim\Slim::getInstance(); //récupère l'instance slim
		$app->response->headers->set('Content-Type', 'application/json'); //réponse au format json
		try{
			$serie = Serie::findOrFail($id); //trouve ou non l'id de la série, si ne trouve pas : créé une erreur
			// $serieSelect = Serie::select('id','ville','map_lat','map_lng','distance','cover')->first();
			$arr = $serie->toArray(); //tableau regroupant les données de la série

			$l = array("href" => "/series/$id/parties"); //tableau contenant le lien vers les parties de la série
			$l1 = array("href" => $app->urlFor('photosSerie', ['id'=>$id] ) ); //tableau contenant le lien vers les photos de la série
			$t = array("parties" => $l); //tableau regroupant "href" et le lien vers les séries
			$t1 = array("photos" => $l1); //tableau regroupant "href" et le lien vers les photos
			$arrFinal = array("serie" => $arr, "links" => $t+$t1); //tableau final regroupant les données de chaque séries et le lien vers ses parties

			echo json_encode($arrFinal); //affichage du tableau final
		}
		catch(\Exception $e){ //création de l'exception si l'id de la série n'est pas trouvé
			$app->response->setStatus(404); //statut de l'erreur 404
			$tab = ["erreur " => "La ressource série $id n'existe pas"]; //tableau json contenant le message d'erreur
			echo json_encode($tab); //affichage du tableau
		}
	}

	public function getSeries() { //obtenir la collection de séries
		$app = \Slim\Slim::getInstance(); //récupère l'instance slim
		$app->response->headers->set('Content-Type', 'application/json'); //réponse au format json

		$series = Serie::select('id','ville','map_lat','map_lng','distance','cover')->get();
		$Serie = array();
		foreach($series as $serie){
			$uri = $app->urlFor('serie', ['id'=> $serie->id]);

			$Serie[] = [ 'serie' => $serie->toArray(),
					 'links' => [ 'self' => [ 'href' => $uri ]]
					];
		}
		$arrayFinal = array("series" => $Serie);

		echo json_encode($arrayFinal);
	}

	public function getPhotosSerie($id) { //obtenir les photos d'une série
		$app = \Slim\Slim::getInstance(); //récupère l'instance slim
		$app->response->headers->set('Content-Type', 'application/json'); //réponse au format json
		try{
			$serie = Serie::findOrFail($id); //trouve ou non l'id de la série, si ne trouve pas : créer une erreur
			$photos = Photo::select("id", "description", "latitude", "longitude", "url", "id_Administration", "id_serie")
								->where("id_serie","=",$id)->orderByRaw('RAND()')->get();
			$Photos = array();
			foreach($photos as $photo){
				$uri = $app->urlFor('serie', ['id' => $id]);
				$Photos[] = [ 'photo' => $photo->toArray(),
						 'links' => [ 'serie' => [ 'href' => $uri ]]
						];
			}
			$arrayFinal = array("Photos" => $Photos);
			echo json_encode($arrayFinal);
		}
		catch(\Exception $e){ //création de l'exception si l'id de l'annonce n'est pas trouvé
			$app->response->setStatus(404); //statut de l'erreur 404
			$tab = ["erreur " => "La ressource série $id n'existe pas"]; //tableau json contenant le message d'erreur
			echo json_encode($tab); //affichage du tableau
		}
	}

}
