<?php

namespace photoLocate\backend\controller;
use \photoLocate\common\model\Administrateur as Administrateur;


class AdminController {

	public function login($rootUri){
		$app = \Slim\Slim::getInstance();


		$data = $app->request->post();
		$message = "";

		session_start();

		if(!isset($_SESSION['admin'])){
			if($data){
				$admin = Administrateur::where("pseudo",$data['nom_adm'])->first();

				if($admin && password_verify($data['pass_adm'],$admin->passwd)){
					$_SESSION["admin"] = $admin;
					header("Location:".$app->redirect('index'));
				}else{
					$message = "Mot de passe incorrect. Essayez de nouveau";
				}
			}
		}else{
			header("Location:".$app->redirect('index'));
		}


		$app->render('login.html.twig',["message"=>$message,
					'root'=> $rootUri,
					'signin'=>[	'href'=>$app->urlFor('signin'),
					'name'=>'Créer un compte']]);
	}

	public function index($rootUri){
		$app = \Slim\Slim::getInstance();

		session_start();

		if(isset($_SESSION['admin'])){
			$app->render('index.html.twig',[
						'root'=> $rootUri,
						'link'=>[	'href'=>$app->urlFor('logout'),
						'name'=>'Logout'],
						'photos'=>$app->urlFor('photos'),
						'series'=>$app->urlFor('series')]);
		}else{
				$app->render('404.html.twig',[	'message'=>'Error 404',
												'root'=> $rootUri,]);
		}
	}

	public function logout($rootUri){
		$app = \Slim\Slim::getInstance();
		session_start();

		if(isset($_SESSION['admin'])){
			session_destroy();
			header("Location:".$app->redirect($app->urlFor('login')));
		}else{
			$app->render('404.html.twig',['message'=>'Error 404',
						'root'=> $rootUri,]);
		}
	}

	public function getAdministrateurs($rootUri) { //104 : affichage de la liste des annonces non validées
		$app = \Slim\Slim::getInstance();
		$app->response->headers->set('Content-Type', 'text/html');

		$administrateurs = Administrateur::select()->get();
		return $administrateurs;
	}

	public function addAdministrateur($rootUri){
		$app = \Slim\Slim::getInstance();

		//on recupere les données du formulaire
		$data = $app->request->post();
		$message = "";

		if($data){
			$count = Administrateur::all()->where("pseudo",$data['nom_adm'])->count();

			if($count == 0){
				if($data['pass_adm'] === $data['conf_pass_adm']){
					$hash = password_hash($data['pass_adm'], PASSWORD_DEFAULT,array('cost'=>12));
					$message = $hash;

					$admin = new Administrateur();
					$admin->pseudo = $data['nom_adm'];
					$admin->passwd = $hash;

					if($admin->save()){
						$message = "Administrateur créé";
						header("Location:".$app->redirect($app->urlFor('login')));
					}else{
						$message = "Erreur de création";
					}
				}else{
					$message = "Le mot de passe de confirmation de correspond pas";
				}
			}else{
				$message = "Le nom d'utilisateur existe déjà";
			}
		}

		$app->render('signin.html.twig',[	'admin'=>"Entra adminstración",
											'root'=> $rootUri,
											'message'=>$message,
											'login'=>['name'=>"Retourner à la page d'accueil",
											'href'=>$app->urlFor('login')]]);
	}
}
