<?php
	namespace photoLocate\backend\controller;

	use \photoLocate\common\model\Photo as Photo;
	use \photoLocate\common\model\Administrateur as  Administrateur;
	use \photoLocate\common\model\Serie as Serie;

class PhotoController{

	public function addPhotoInterface($rootUri){
		$app = \Slim\Slim::getInstance();
		$app->response->headers->set('Content-type', 'text/html');
		$series = Serie::select('ville')->get(); //récupérer les séries pour les afficher dans le menu déroulant du formulaire
		
		session_start();
		if(isset($_SESSION['admin'])){
			$app->render( 'addPhotos.html.twig',['root' => $rootUri,
																					'series' => $series,
																					'accueil'=>['href'=>$app->urlFor('index'),
																											'name'=>'Accueil'],
																					'link'=>[	'href'=>$app->urlFor('logout'),
																								'name'=>'Logout']] ); //appel de twig
		}else{
				$app->render('404.html.twig',[	'message'=>'Error 404',
											'accueil'=>$app->urlFor('login'),
											'root'=> $rootUri,]);
		}
	}

	public function addPhoto($rootUri){
		$app = \Slim\Slim::getInstance();
		$app->response->headers->set('Content-type', 'text/html');

		session_start();
		
		if(isset($_SESSION['admin'])){			
			$dataForm = $app->request->post();
				// On récupère le  contenu du formulaire
				$pseudo =$dataForm["pseudo"];
				$ville = $dataForm["serie"];
				$id_serie = Serie::select('id')->where('ville','=',$ville)->first();
				
				$series = Serie::select('ville')->get(); //récupérer les séries pour les afficher dans le menu déroulant du formulaire

				$app->render( 'addPhotos.html.twig',['root' => $rootUri, 'series' => $series] ); //appel de twig
			try{
				// on choisit dans quel dossier sera stocké le document recueilli
				$image = new \Upload\Storage\FileSystem('../play/web/images/villes/'.$ville);

				// on récupère le fichier qui a été uploadé dans le formulaire
				$file = new \Upload\File('file',$image);

				$name =uniqid();
				$file->setName($name);

				//tout ce qui concerne le fichier
				$data = array(
					'name'       => $file->getNameWithExtension(),
					'extension'  => $file->getExtension(),
					'mime'       => $file->getMimetype(),
					'size'       => $file->getSize(),
					'md5'        => $file->getMd5(),
					'dimensions' => $file->getDimensions()
				);
				try {
					// Success!
					$file->upload();
				} catch (\Exception $e) {
					// Fail!
					$errors = $file->getErrors();
				}
				$admin = Administrateur::select('id')->where('pseudo','=',$pseudo)->first();

				$photo = new Photo();
				$photo -> description = $data["name"];
				$photo -> latitude = $dataForm["latitude"];
				$photo -> longitude = $dataForm["longitude"];
				$photo -> url =$data['name'];
				$photo -> id_Administration = $admin->id;
				
				//récupère l'id correspondant à la série choisi dans le formulaire
				$serie_id = Serie::select('id')->where('ville','=',$ville)->first();				
				$serie_id_name = $serie_id->id;
				
				$photo -> id_serie = $serie_id_name;
				$photo -> save();
				$id = $photo->id;
			}

			catch(\Exception $e){
				$app->response->setStatus(404); //statut de l'erreur 404
					$tabErreur = ["erreur " => "La photo n'a pas pu être ajoutée, l'administrateur n'a pas été trouvé."]; //tableau json contenant le message d'erreur
					echo json_encode($tabErreur); //affichage du tableau
			}
		} else {
			$app->render('404.html.twig',['message'=>'Error 404',
						'root'=> $rootUri,]);
		}
	}
}
?>
