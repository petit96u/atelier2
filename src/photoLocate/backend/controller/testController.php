<?php

namespace photoLocate\backend\controller;

use \photoLocate\common\model\Serie as Serie;

class testController { 
	
	public function index($rootUri) {  //index
		$app = \Slim\Slim::getInstance();
		$app->response->headers->set('Content-Type', 'text/html'); 
		
		$annonces = 'test';
		
		$app->render( 'index.html.twig',[ 'annonce' => $annonces, 'root' => $rootUri ] ); //appel de twig
	}
	
	public function addPhotos($rootUri) {  //ajouter photo à la série existante
		$app = \Slim\Slim::getInstance();
		$app->response->headers->set('Content-Type', 'text/html'); 
		
		$annonces = 'test';
		
		$series = Serie::select('ville')->get(); //récupérer les séries pour les afficher dans le menu déroulant du formulaire
			
		$app->render( 'addPhotos.html.twig',[ 'annonce' => $annonces, 'root' => $rootUri, 'series' => $series] ); //appel de twig
	}
	
	public function addSerie($rootUri) {  //ajouter une série
		$app = \Slim\Slim::getInstance();
		$app->response->headers->set('Content-Type', 'text/html'); 
		
		$annonces = 'test';
			
		$app->render( 'addSerie.html.twig',[ 'annonce' => $annonces, 'root' => $rootUri] ); //appel de twig
	}	
}