<?php

namespace photoLocate\backend\controller;

use \photoLocate\common\model\Serie as Serie;

class SerieController {

	public function addSerieAffichage($rootUri) {  //affichage d'ajouter une s�rie
		$app = \Slim\Slim::getInstance();
		$app->response->headers->set('Content-Type', 'text/html');

		session_start();

		if(isset($_SESSION['admin'])){
			$app->render( 'addSerie.html.twig',['root' => $rootUri,
																					'accueil'=>['href'=>$app->urlFor('index'),
																											'name'=>'Accueil'],
																					'link'=>[	'href'=>$app->urlFor('logout'),
																								'name'=>'Logout']] ); //appel de twig
		}else{
				$app->render('404.html.twig',[	'message'=>'Error 404',
											'accueil'=>$app->urlFor('login'),
											'root'=> $rootUri,]);
		}
	}

	public function addSerie($rootUri){ //ajouter une série
		$app = \Slim\Slim::getInstance();
		$app->response->headers->set('Content-type', 'text/html');

		$dataForm = $app->request->post(); //récupère le contenu du formulaire

		try{			
			$ville = $dataForm["ville"];
			$villeMaj = ucfirst($ville); //met 1ère lettre de la ville en majuscule
			
			//vérifie si la ville est dans la base
			$villes = Serie::where('ville','=',$villeMaj)->get(); 
			//$villeNom = $villes->ville;
			if( empty($villeNom) ){ //si la ville n'existe pas dans la base
				$serie = new Serie();
				$serie -> ville = $villeMaj;
				$serie -> map_lat = $dataForm["latitude"];
				$serie -> map_lng = $dataForm["longitude"];
				$serie -> distance = $dataForm["distance"];
				$serie -> save();
				$idSerie = $serie->id; //récupère l'id de la série qui vient d'être créé
				
				//création du dossier pour les images de la série
				$structure = '../play/web/images/villes/'.$villeMaj;
				if (!mkdir($structure, 0777, true)) {
					die('Echec lors de la création du répertoire');
				}						
				
				$app->render( 'addSerie.html.twig',['msg' => 'La série a bien été ajouté', 'root' => $rootUri] ); //renvoie la page d'ajout de série								
			}
			else{}			
		}	
		catch(\Exception $e){
			echo "Cette serie $villeMaj existe deja";		
		}
	}
}
