var serie = {
    modules:{}
}

serie.modules.app = (function(){
    return{
        run: function(){

          $.ajax({
              url:'../api/series',
              type: 'GET',
              headers:{
                  'Authorization' : sessionStorage.getItem("token")},
              success : function(data){
                  data.series.forEach(function(e){
                      var link = document.createElement("a");
                      link.setAttribute("href","#")

                      var image = document.createElement("img");
                      image.setAttribute("src",'web/images/'+e.serie.cover);
                      image.setAttribute("class","col-sm-4 col-sm-offset-1 col-xs-6");

                      var galerie = document.getElementById("page-choix-ville");

                      galerie.appendChild(link);
                      link.appendChild(image);

                      image.onclick = function(){
                        $.ajax({
                            url:'../api/parties/'+sessionStorage.getItem("token"),
                            type: 'PUT',
                            headers:{
                                'Authorization' : sessionStorage.getItem("token")},
                            data:{
                                api_status:2,
                                api_id_serie:e.serie.id
                            }
						});
						window.location = '../play/photoLocate.html';
					}
                  });
                }
            });
        }
    }
});

window.onload = serie.modules.app().run();