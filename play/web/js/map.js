var photoLocate = {
	modules:{}
};

photoLocate.modules.actions = (function() {
	var centi = 0;
	var secon = 0;
	
	return {
		init : function(d) { //initaliser la distance / marge d'erreur possible
			distanceInit = d;
		},
		
		mapInit: function(lat, lng, latMap, lngMap, tab, cumul, tabLat, tabLng, ville) { //initalise la carte
			var map = L.map('map').setView([latMap, lngMap], 15);
				L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
				attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
				maxZoom: 15,
				minZoom: 15,
				id: 'haas36.p1gg3j9j',
				accessToken: 'pk.eyJ1IjoiaGFhczM2IiwiYSI6ImNpazJ2OGp2cjAzNDl3NWtwM3dsYzlhcGIifQ.b_mpTO00mw5GT5tpkSSfXw'
			}).addTo(map);
			var popup = L.popup();
			var fixedMarker = L.marker([lat, lng]);
			var fc = fixedMarker.getLatLng();
			map.dragging.disable();
			map.touchZoom.disable();
			map.doubleClickZoom.disable();
			map.scrollWheelZoom.disable();
			map.keyboard.disable();
			function onMapClick(e) {
				var c = e.latlng;
				var distanceTotal = (fc.distanceTo(c)).toFixed(0);
				photoLocate.modules.actions.ptsDistance(distanceTotal, tab, cumul, lat, lng, latMap, lngMap, tabLat, tabLng, ville);
			}
			map.on('click', onMapClick);
		},
		
		ptsDistance: function(distanceTotal, tab, cumul, lat, lng, latMap, lngMap, tabLat, tabLng, ville) { //calcul des points en fonction de la distance
			var pts = 0
			if(distanceTotal < distanceInit) {
				pts += 5;
			} else {
				if(distanceTotal < 2*distanceInit) {
					pts += 3;
				} else {
					if(distanceTotal < 3*distanceInit) {
						pts += 1;
					} else {
						pts += 0;
					}
				}
			}
			photoLocate.modules.actions.ptsTime(tab, pts, cumul, lat, lng, latMap, lngMap, tabLat, tabLng, ville);
		},		
		
		ptsTime: function(tab, pts, cumul, lat, lng, latMap, lngMap, tabLat, tabLng, ville) { //calcul des points en fonction du temps
			if(secon < 2) {
				pts = 4*pts;
			} else {
				if(secon > 2 && secon < 5) {
					pts = 2*pts;
				} 
				else {
					if(secon > 10 && centi > 0) {
						pts = 0*pts;
					}
				}
			}		
			cumul += pts;
			photoLocate.modules.app.displayStatus(cumul);
			photoLocate.modules.app.next(tab, cumul, lat, lng, latMap, lngMap, tabLat, tabLng, ville);
		},
		
		timer: function() { //timer
				centi++;
				if (centi>9){ 
					centi=0;
					secon++;
				}
				compte = setTimeout('photoLocate.modules.actions.timer(false)',100);
				photoLocate.modules.app.displayTimer(centi, secon);
		
		},
		
		rasee: function() { //pour arrêter le timer
			clearTimeout(compte);
			centi=0;
			secon=0;
		}
	}
})();

photoLocate.modules.app = (function() {
	var actions = photoLocate.modules.actions;
	var incrementationImg = 0;
	var incrementationMap = 0;
	var incrementationFinale = 1;
	
	Object.size = function(tab) {
		var size = 0, key;
		for (key in tab) {
			if(tab.hasOwnProperty(key));
				size++;
			}
			return size;
	};
		
	
	return {
	
		run: function() {
			$.ajax({
				url:'../api/parties/'+sessionStorage.getItem("token"),
				type: 'GET',
				success : function(data){
					var idSerie = data.Partie.id_serie;
					console.log(idSerie);
					$.ajax({
						url:'../api/series/'+idSerie+'/photos',
						type: 'GET',
						headers:{'Authorization' : sessionStorage.getItem("token")},
						success : function(data){
							var tab = {};
							for (i = 0; i < data.Photos.length; i++) {
								var url = data.Photos[i].photo.url;
								tab[i] = url;
							}
							
							
							var imgLocal = localStorage.setItem("imgLocal",tab);
							var cumul = 0;
							photoLocate.modules.app.displayStatus(cumul);
							
							var tabLat = {};
							var tabLng = {};
							for (j = 0; j < data.Photos.length; j++) { 
								var lat = data.Photos[j].photo.latitude;
								var lng = data.Photos[j].photo.longitude;
								tabLat[j] = lat;
								tabLng[j] = lng;
							}
							$.ajax({
								url:'../api/series/'+idSerie,
								type: 'GET',
								headers:{'Authorization' : sessionStorage.getItem("token")},
								success : function(data){
									var latMap = data.serie.map_lat;
									var lngMap = data.serie.map_lng;
									var ville = data.serie.ville;
									var distance = data.serie.distance;
								
									photoLocate.modules.app.nextImage(tab, cumul, ville);
									photoLocate.modules.app.nextMap(lat, lng, latMap, lngMap, tab, cumul, tabLat, tabLng, ville);
									actions.init(distance);
								}
							});
						}
					});
				}
            });
		},
		
		displayTimer : function(centi, secon) {
			$("#timer").empty();
			var $myDiv = $("<li>").appendTo("#timer");
			$myDiv.text("Temps : "+ secon + " secondes " + centi + " dixièmes ");
		},
		
		displayStatus : function(cumul) {
			$("#status").empty();
			var myDiv = $("<li>").appendTo("#status");
			myDiv.text("Points : "+cumul);
		},
		
		next : function(tab, cumul, lat, lng, latMap, lngMap, tabLat, tabLng, ville) {
			actions.rasee();
			var size = Object.size(tab);
			if(size == incrementationFinale) {
				photoLocate.modules.app.modal(cumul);
			} else {
				incrementationFinale ++;
				$("#nextImage").empty();
				var myButton = $("<button>").appendTo("#nextImage");
				myButton.text("Suivante");
				$(myButton).click(function() { 
					photoLocate.modules.app.nextImage(tab, cumul, ville);
					photoLocate.modules.app.nextMap(lat, lng, latMap, lngMap, tab, cumul, tabLat, tabLng, ville);
				});
			}
		},
		
		nextImage : function(tab, cumul, ville) {
			$("#nextImage").empty();
			var tab = tab;
			var url = tab[incrementationImg];
			localStorage.setItem("photo",url);
			var photo = localStorage.getItem("photo");
			photoLocate.modules.app.displayImage(photo, tab, cumul, ville);
		},
		
		nextMap : function(lat, lng, latMap, lngMap, tab, cumul, tabLat, tabLng, ville) {
			var tabLat = tabLat;
			var tabLng = tabLng;
			var lat = tabLat[incrementationMap];
			var lng = tabLng[incrementationMap];
			incrementationMap ++;
			actions.mapInit(lat, lng, latMap, lngMap, tab, cumul, tabLat, tabLng, ville);
		},
		
		displayImage : function(url, tab, cumul, ville) {
			$("#container-map-img").empty();
			$nextImage = $("<img src='web/images/villes/"+ville+"/"+url+"'>").addClass("col-sm-4").appendTo("#container-map-img");
			incrementationImg ++;
			photoLocate.modules.app.displayMap();
		},
		
		displayMap : function() {
			$("<div>").attr('id', 'map').addClass("col-sm-8").appendTo("#container-map-img");
			actions.timer(false);
		},
		
		modal : function(cumul) {						   
			//Faire apparaitre la pop-up et ajouter le bouton de fermeture
			$('#popup').fadeIn().css({ 'width': 500}).prepend('<a href="#" class="close"><img src="web/images/close_pop.png" class="btn_close" title="Fermer la fenêtre" alt="Close" /></a>');
				
			//Récupération du margin, qui permettra de centrer la fenêtre - on ajuste de 80px en conformité avec le CSS
			var popMargTop = ($('#popup').height() + 80) / 2;
			var popMargLeft = ($('#popup').width() + 80) / 2;
				
			//Applique des marges au popup
			$('#popup').css({ 
				'margin-top' : -popMargTop,
				'margin-left' : -popMargLeft
			});
			$("#popup-score-total").text(cumul);
			
			$("#add-score").click(function(){
				$.ajax({
					url:'../api/parties/'+sessionStorage.getItem("token"),
					type: 'GET',
					success : function(data){
						var id_serie = data.Partie.id_serie;
						var joueur = data.Partie.joueur;
						var partie_id = data.Partie.id;
						$.ajax({
							url:'../api/parties/'+sessionStorage.getItem("token"),
							type: 'PUT',
							headers:{'Authorization' : sessionStorage.getItem("token"), 'Content-Type': 'application/x-www-form-urlencoded'},
                            data:{
                                api_score:cumul,
                                api_status:2,
                                api_id_serie:id_serie
                            },
							success : function(data){
								$.ajax({
									url:'../api/scores',
									type: 'POST',
									headers:{'Authorization' : sessionStorage.getItem("token")},
									data:{
										joueur:joueur,
										score:cumul,
										partieId:partie_id
									},
									success : function(data){
										var p = $("<p>").appendTo("#popup");
										p.text("Tu as ajouté ton score");
									}
								});
							}
						});
					}
				});
			});
			
			//Apparition du fond - .css({'filter' : 'alpha(opacity=80)'}) pour corriger les bogues d'anciennes versions de IE
			$('body').append('<div id="fade"></div>');
			$('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn();
				
			return false;

			//ferme le popup
			$('body').on('click', 'a.close, #fade', function() { //Au clic sur le body...
				$('#popup').addClass("none");
			
				// $('#fade , .popup_block').fadeOut(function() {
					// $('#fade, a.close').remove();  
			// }); //...ils disparaissent ensemble
				
				return false;
			});
		}
	}
})();

window.onload = photoLocate.modules.app.run;