var partie = {
    modules:{}
};

partie.modules.app = (function(){
    //var actions = partie.modules.actions;
    var token = '';
    return{
        run: function(){
            button = document.getElementById('b1');

            button.onclick = function(){
                var pseudo = document.getElementById('pseudo').value;
                console.log(pseudo);

                $.post('../api/parties',{
                  "api_joueur" : pseudo,
                  "api_id_serie" : 0
                }).done(function(data){
                    this.token = data.token;
                    if(window.sessionStorage){
                        sessionStorage.setItem("token",data.token);
                    }
                    window.location = '../play/villes.html';
                }).fail(function(){
                    var error = document.getElementById("error");
                    var p = document.createElement("p");
                    p.setAttribute("id","p_error");
                    var p2 = document.getElementById("p_error");
                    var texte = document.createTextNode("Le pseudo ajouté existe déjà");

                    p.appendChild(texte);
                    error.replaceChild(p,p2);
                    console.log("Error 404");
                });

            }
        },
    }
});

window.onload = partie.modules.app().run();